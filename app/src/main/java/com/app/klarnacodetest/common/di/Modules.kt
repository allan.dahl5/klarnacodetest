package com.app.klarnacodetest.common.di

import android.app.Activity
import androidx.room.Room
import coil.ImageLoader
import coil.fetch.VideoFrameFileFetcher
import coil.fetch.VideoFrameUriFetcher
import coil.util.CoilUtils
import com.app.klarnacodetest.BuildConfig
import com.app.klarnacodetest.common.data.database.AppDatabase
import com.app.klarnacodetest.common.data.network.api.WeatherApi
import com.app.klarnacodetest.common.data.network.infrastructure.ApiClient
import com.app.klarnacodetest.common.data.network.infrastructure.UUIDAdapter
import com.app.klarnacodetest.common.data.network.repository.WeatherRepository
import com.app.klarnacodetest.common.data.prefs.SharedPref
import com.app.klarnacodetest.common.utils.AppLoader
import com.app.klarnacodetest.ui.main.MainActivityViewModel
import com.app.klarnacodetest.ui.main.weather.WeatherFragmentViewModel
import com.squareup.moshi.Moshi
import com.squareup.moshi.adapters.Rfc3339DateJsonAdapter
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.HttpUrl
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import java.util.*

var appModules = module {

    single {
        ImageLoader.Builder(get())
            .componentRegistry {
                add(VideoFrameFileFetcher(get()))
                add(VideoFrameUriFetcher(get()))
            }
            .okHttpClient {
                OkHttpClient.Builder()
                    .cache(CoilUtils.createDefaultCache(get()))
                    .build()
            }
            .build()
    }

    single {
        Moshi.Builder()
            .add(Date::class.java, Rfc3339DateJsonAdapter().nullSafe())
            .add(UUIDAdapter())
            .addLast(KotlinJsonAdapterFactory())
            .build()
    }

    single { SharedPref(androidContext(), get()) }

    single {
        Room.databaseBuilder(get(), AppDatabase::class.java, "klarna-database")
            .build()
    }

    factory { (context: Activity) -> AppLoader(context) }
}

val viewModelModule = module {
    viewModel { MainActivityViewModel() }
    viewModel { WeatherFragmentViewModel() }
}

val databaseModule = module {
    single { (get() as AppDatabase).getWeatherLocalDao() }
}
val networkModules = module {
    // Register ApiClient Instance
    single { provideApiClient() }

    // Register WeatherApi Instance
    single { provideWeatherAPI(get()) }
}

val repoModules = module {
    single { WeatherRepository(get(), get()) }
}

fun provideApiClient(): ApiClient {
    return ApiClient(
        baseUrl = BuildConfig.BACKEND_HOST,
        okHttpClientBuilder = OkHttpClient
            .Builder()
            .addInterceptor { chain ->
                val original = chain.request()
                val originalHttpUrl: HttpUrl = original.url

                val url = originalHttpUrl.newBuilder()
                    .addQueryParameter("access_key", BuildConfig.ACCESS_KEY)
                    .build()

                // Request customization: add request headers
                val requestBuilder = original.newBuilder()
                    .url(url)

                val request = requestBuilder.build()
                return@addInterceptor chain.proceed(request)
            }
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
    )
}

fun provideWeatherAPI(apiClient: ApiClient): WeatherApi {
    return apiClient.createService(WeatherApi::class.java)
}
