package com.app.klarnacodetest.common.data.database.entity

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.app.klarnacodetest.common.data.network.model.ForecastInfo

@Entity
class WeatherLocal {
    @PrimaryKey
    var name: String = ""
    var weather_code: Int = 0
    var current_temp: Double = 0.0
    var current_wind: Double = 0.0
    var current_feel: Double = 0.0
    var forecast: List<ForecastInfo>? = null
}