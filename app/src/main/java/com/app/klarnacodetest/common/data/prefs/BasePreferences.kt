package com.app.klarnacodetest.common.data.prefs

import android.content.Context
import android.content.SharedPreferences

open class BasePreferences(context: Context) {

    private var iSharedPrefs: SharedPreferences =
        context.getSharedPreferences("prefs_app", Context.MODE_PRIVATE)

    //region Boolean
    protected fun setBoolean(key: String, value: Boolean) =
        iSharedPrefs.edit().putBoolean(key, value).apply()

    protected fun getBoolean(key: String, defaultValue: Boolean) =
        iSharedPrefs.getBoolean(key, defaultValue)
    //endregion

    //region String
    protected fun setString(key: String, value: String?) =
        iSharedPrefs.edit().putString(key, value).apply()

    protected fun getString(key: String) = iSharedPrefs.getString(key, "")
    //endregion

    //region Integer
    protected fun setLong(key: String, value: Long) =
        iSharedPrefs.edit().putLong(key, value).apply()

    protected fun getLong(key: String) = iSharedPrefs.getLong(key, 0L)
    //endregion

    //region Integer
    protected fun setInt(key: String, value: Int) = iSharedPrefs.edit().putInt(key, value).apply()

    protected fun getInt(key: String) = iSharedPrefs.getInt(key, 0)
    //endregion

    // <editor-fold desc="Clear App Data">
    fun clearAppData() {
    }
    // </editor-fold>

    // <editor-fold desc="Clear All Preferences">
    fun clearPreferences() {
        iSharedPrefs.edit().clear().apply()
    }
    // </editor-fold>
}
