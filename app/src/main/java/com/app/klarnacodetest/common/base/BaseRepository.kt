package com.app.klarnacodetest.common.base

import com.app.klarnacodetest.common.data.ApiError
import com.app.klarnacodetest.common.data.ApiEvent
import com.app.klarnacodetest.common.data.ApiSuccess
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import retrofit2.HttpException
import retrofit2.Response

abstract class BaseRepository {

    protected suspend fun <T> callApi(apiCall: suspend () -> Response<T>): ApiEvent<T> {
        return withContext(Dispatchers.IO) {
            try {
                val response = apiCall()
                // Listen APIs Response/Failure in @Dispatchers.Main thread
                withContext(Dispatchers.Main) {
                    ApiSuccess(response.body())
                }
            } catch (throwable: Throwable) {
                when (throwable) {
                    is HttpException -> ApiError(throwable)
                    else -> ApiError(throwable)
                }
            }
        }
    }
}
