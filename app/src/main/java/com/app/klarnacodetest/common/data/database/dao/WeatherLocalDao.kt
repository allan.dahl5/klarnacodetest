package com.app.klarnacodetest.common.data.database.dao

import androidx.room.*
import com.app.klarnacodetest.common.data.database.entity.WeatherLocal

@Dao
interface WeatherLocalDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertWeather(items: WeatherLocal)

    @Update
    suspend fun updateWeather(item: WeatherLocal)

    @Delete
    suspend fun deleteWeather(item: WeatherLocal)

    @Query("SELECT * FROM WeatherLocal WHERE name=:query")
    suspend fun getWeather(query: String): List<WeatherLocal>
}
