package com.app.klarnacodetest.common.data.database

import androidx.room.TypeConverter
import com.app.klarnacodetest.common.data.network.model.ForecastInfo
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import org.koin.core.component.KoinApiExtension
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject

@KoinApiExtension
class RoomConverters : KoinComponent {

    val moshi: Moshi by inject()

    private val typeList = Types.newParameterizedType(List::class.java, ForecastInfo::class.java)
    private val jsonAdapter: JsonAdapter<List<ForecastInfo>> = moshi.adapter(typeList)

    @TypeConverter
    fun toForecastInfoList(jsonStr: String?): List<ForecastInfo> {
        return if (jsonStr == null) listOf()
        else jsonAdapter.fromJson(jsonStr).orEmpty()
    }

    @TypeConverter
    fun fromForecastInfoList(list: List<ForecastInfo>): String = jsonAdapter.toJson(list)
}