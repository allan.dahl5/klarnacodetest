package com.app.klarnacodetest.common.data.network.model

data class WeatherInfo(
    val request: WeatherRequest?,
    val location: WeatherLocation?,
    val current: WeatherCurrent?,
    val forecast: Map<String, ForecastInfo>?
)

data class WeatherRequest(
    val type: String?,
    val query: String?,
    val language: String?,
    val unit: String?,
)

data class WeatherLocation(
    val name: String?,
    val country: String?,
    val region: String?,
    val lat: String?,
    val lon: String?,
    val timezone_id: String?,
    val localtime: String?,
    val localtime_epoch: Long?,
    val utc_offset: String?,
)

data class WeatherCurrent(
    val observation_time: String?,
    val temperature: Double?,
    val weather_code: Int?,
    val weather_icons: List<String>?,
    val weather_descriptions: List<String>?,
    val wind_speed: Double?,
    val wind_degree: Double?,
    val wind_dir: String?,
    val pressure: Double?,
    val precip: Double?,
    val humidity: Double?,
    val cloudcover: Double?,
    val feelslike: Double?,
    val uv_index: Double?,
    val visibility: Double?,
    val is_day: String?,
)

data class ForecastInfo(
    val date: String?,
    val date_epoch: Long?,
    val astro: Astro?,
    val mintemp: String?,
    val maxtemp: String?,
    val avgtemp: String?,
    val totalsnow: String?,
    val sunhour: String?,
    val uv_index: String?,
)

data class Astro(
    val sunrise: String?,
    val sunset: String?,
    val moonrise: String?,
    val moonset: String?,
    val moon_phase: String?,
    val moon_illumination: Int?,
)