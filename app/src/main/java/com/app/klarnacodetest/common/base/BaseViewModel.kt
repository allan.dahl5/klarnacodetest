package com.app.klarnacodetest.common.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.app.klarnacodetest.common.data.*
import com.app.klarnacodetest.common.data.network.repository.WeatherRepository
import com.app.klarnacodetest.common.data.prefs.SharedPref
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import org.koin.core.component.KoinComponent
import org.koin.core.component.inject
import timber.log.Timber
import kotlin.coroutines.CoroutineContext

open class BaseViewModel : ViewModel(), KoinComponent, CoroutineScope {

    private val errorHandler = CoroutineExceptionHandler { _, throwable -> Timber.w(throwable) }
    private val vmJob = Job()

    override val coroutineContext: CoroutineContext = Dispatchers.IO + vmJob + errorHandler

    val sharedPref: SharedPref by inject()

    val weatherRepo: WeatherRepository by inject()

    /**
     * Used when internet is not available during API call
     * */
    private val _networkErrors = MutableLiveData<String>()
    val networkErrors: LiveData<String>
        get() = _networkErrors

    /**
     * Used when there is API error during API call
     * */
    private val _errors = MutableLiveData<Throwable>()
    val errors: LiveData<Throwable>
        get() = _errors

    /**
     * Used to display loader during API call
     * */
    private val _loader = MutableLiveData<Boolean>()
    val loader: LiveData<Boolean>
        get() = _loader

    /**
     * Used to process the API response.
     * */
    protected inline fun <T> processDataEvent(
        result: Event<T>,
        onError: (value: Throwable) -> Unit = {},
        onSuccess: (value: T) -> Unit
    ) {
        when (result) {
            is ApiEvent -> {
                handleApiEvent(result, onSuccess, onError)
            }

            is DatabaseEvent -> {
                handleDatabaseEvent(result, onSuccess, onError)
            }
        }
    }

    /**
     * Used for processing local database response
     * */
    protected inline fun <T> handleDatabaseEvent(
        result: DatabaseEvent<T>,
        onSuccess: (value: T) -> Unit,
        onError: (value: Throwable) -> Unit
    ) {
        when (result) {
            is DatabaseError -> {
                showError(result.error)
                onError(result.error)
            }
            is DatabaseSuccess -> {
                onSuccess(result.data)
            }
        }
    }

    /**
     * Used for processing server response
     * */
    protected inline fun <T> handleApiEvent(
        result: ApiEvent<T>,
        onSuccess: (value: T) -> Unit,
        onError: (value: Throwable) -> Unit
    ) {
        try {
            when (result) {
                is ApiError -> {
                    dismissLoader()
                    showError(result.error)
                    onError(result.error)
                }
                is ApiSuccess -> {
                    dismissLoader()
                    result.response?.let { onSuccess(it) }
                }
                is ApiNetworkError -> {
                    dismissLoader()
                    showNetworkError()
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    protected open fun showError(error: Throwable) {
        _errors.value = error
    }

    protected open fun showNetworkError() {
        _networkErrors.postValue("No Internet Connection")
    }

    /**
     * This logic is responsible for displaying loader for single time even during multiple api call.
     * */
    private var loaderCount = 0
    fun dismissLoader() {
        loaderCount--
        if (loaderCount <= 0) {
            loaderCount = 0
            _loader.postValue(false)
        }
    }

    /**
     * This logic is responsible for displaying loader for single time even during multiple api call.
     * */
    fun displayLoader() {
        loaderCount++
        if (loaderCount == 1) _loader.postValue(true)
    }
}
