package com.app.klarnacodetest.common.data

sealed class Event<out T>
sealed class ApiEvent<out T> : Event<T>()
sealed class DatabaseEvent<out T> : Event<T>()

class ApiSuccess<out T>(val response: T?) : ApiEvent<T>()
class ApiError(val error: Throwable) : ApiEvent<Nothing>()
object ApiNetworkError : ApiEvent<Nothing>()

class DatabaseSuccess<out T>(val data: T) : DatabaseEvent<T>()
class DatabaseError<out T>(val error: java.lang.Exception) : DatabaseEvent<T>()
