package com.app.klarnacodetest.common.base

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import com.app.klarnacodetest.common.data.prefs.SharedPref
import com.app.klarnacodetest.common.utils.AppLoader
import com.squareup.moshi.Moshi
import kotlinx.coroutines.channels.ReceiveChannel
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

abstract class BaseActivity<VB : ViewDataBinding>(private val layoutRes: Int) :
    AppCompatActivity() {

    val moshi: Moshi by inject()
    val sharedPref: SharedPref by inject()

    protected val handler: Handler by lazy { Handler(Looper.getMainLooper()) }
    protected lateinit var binding: VB
    protected val listSubscription = ArrayList<ReceiveChannel<*>>()
    private val appLoader: AppLoader by inject { parametersOf(this) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, layoutRes)
    }

    override fun onDestroy() {
        super.onDestroy()
        listSubscription.forEach { it.cancel() }
    }

    protected fun updateLoaderUI(isShow: Boolean) {
        if (isShow) appLoader.show() else appLoader.dismiss()
    }

    protected fun showMessage(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show()
    }
}
