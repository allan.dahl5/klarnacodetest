package com.app.klarnacodetest.common.utils

/**
 * Declare all the App Constants here
 * For Ex: EXTRAS_FOO, REQ_CODE_FOO, etc.
 * */
const val REQ_CODE_CAPTURE = 101

const val ARGS_CITY = "ARGS_CITY"