package com.app.klarnacodetest.common.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.app.klarnacodetest.common.data.database.dao.WeatherLocalDao
import com.app.klarnacodetest.common.data.database.entity.WeatherLocal
import org.koin.core.component.KoinApiExtension

@KoinApiExtension
@Database(
    entities = [WeatherLocal::class],
    version = 1
)
@TypeConverters(RoomConverters::class)
abstract class AppDatabase : RoomDatabase() {
    abstract fun getWeatherLocalDao(): WeatherLocalDao
}
