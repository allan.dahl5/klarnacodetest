package com.app.klarnacodetest.common.data.network.repository

import com.app.klarnacodetest.common.base.BaseRepository
import com.app.klarnacodetest.common.data.*
import com.app.klarnacodetest.common.data.database.dao.WeatherLocalDao
import com.app.klarnacodetest.common.data.database.entity.WeatherLocal
import com.app.klarnacodetest.common.data.network.api.WeatherApi
import com.app.klarnacodetest.common.data.network.model.WeatherInfo

class WeatherRepository(private val iApi: WeatherApi, private val weatherDao: WeatherLocalDao) :
    BaseRepository() {

    suspend fun getCurrent(query: String): ApiEvent<WeatherInfo> = callApi { iApi.current(query) }

    suspend fun getAndUpdateForecast(
        query: String,
        forecastDays: Int,
        hourly: Int
    ): Event<WeatherLocal> {
        return when (val apiEvent = callApi { iApi.forecast(query, forecastDays, hourly) }) {
            is ApiSuccess -> {
                if (apiEvent.response != null) {
                    val weatherLocal = WeatherLocal().apply {
                        name = apiEvent.response.location?.name.orEmpty()
                        weather_code = apiEvent.response.current?.weather_code ?: 0
                        current_temp = apiEvent.response.current?.temperature ?: 0.0
                        current_wind = apiEvent.response.current?.wind_speed ?: 0.0
                        current_feel = apiEvent.response.current?.feelslike ?: 0.0
                        forecast = apiEvent.response.forecast?.values?.toMutableList()
                    }
                    weatherDao.insertWeather(weatherLocal)
                    ApiSuccess(weatherLocal)
                } else {
                    ApiError(Exception("Unable to fetch"))
                }
            }
            is ApiError -> apiEvent
            else -> DatabaseError(Exception("Unable to fetch"))
        }
    }

    suspend fun getCachedForecast(query: String): DatabaseEvent<WeatherLocal> {
        val list = weatherDao.getWeather(query)
        return if (list.isNullOrEmpty()) DatabaseError(Exception("No Cached Data found"))
        else DatabaseSuccess(list.first())
    }
}
