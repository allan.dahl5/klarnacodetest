package com.app.klarnacodetest.common.data.prefs

import android.content.Context
import com.squareup.moshi.Moshi

class SharedPref(context: Context, private val moshi: Moshi) : BasePreferences(context) {

    private inline fun <reified T> toJson(value: T?) =
        if (value == null) null else moshi.adapter(T::class.java).toJson(value)

    private inline fun <reified T> fromJson(value: String?) =
        if (value.isNullOrEmpty()) null else moshi.adapter(T::class.java).fromJson(value)

    var authToken: String?
        get() = getString(this::authToken.name)
        set(value) {
            setString(this::authToken.name, value)
        }

    var username: String?
        get() = getString(this::username.name)
        set(value) {
            setString(this::username.name, value)
        }

    fun logOut() {
        this.clearPreferences()
    }
}
