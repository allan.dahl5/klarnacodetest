package com.app.klarnacodetest.common.utils

import android.app.Activity
import android.app.Dialog
import com.app.klarnacodetest.R

class AppLoader(host: Activity) {
    private var loader: Dialog = Dialog(host)

    init {
        loader.setContentView(R.layout.dialog_app_loader)
        loader.setCancelable(true)
        loader.window?.setBackgroundDrawable(null)
        loader.window?.setDimAmount(0f)
        loader.create()
    }

    fun show() {
        loader.show()
    }

    fun dismiss() {
        loader.dismiss()
    }
}
