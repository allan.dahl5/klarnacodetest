package com.app.klarnacodetest.common.data.network.api

import com.app.klarnacodetest.common.data.network.model.WeatherInfo
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface WeatherApi {

    @GET("current")
    suspend fun current(@Query("query") query: String): Response<WeatherInfo>

    @GET("forecast")
    suspend fun forecast(
        @Query("query") query: String,
        @Query("forecast_days") forecastDays: Int,
        @Query("hourly") hourly: Int
    ): Response<WeatherInfo>
}
