package com.app.klarnacodetest.ui.main.weather

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import com.app.klarnacodetest.R
import com.app.klarnacodetest.common.base.BaseFragment
import com.app.klarnacodetest.common.data.database.entity.WeatherLocal
import com.app.klarnacodetest.common.utils.ARGS_CITY
import com.app.klarnacodetest.databinding.FragmentWeatherBinding
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber

class WeatherFragment private constructor() :
    BaseFragment<FragmentWeatherBinding>(R.layout.fragment_weather) {

    private val viewModel by viewModel<WeatherFragmentViewModel>()
    private var city = ""
    private val adapter by lazy { ForecastAdapter() }

    companion object {
        fun newInstance(city: String): WeatherFragment {
            return WeatherFragment().apply { arguments = bundleOf(Pair(ARGS_CITY, city)) }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let { city = it[ARGS_CITY] as String }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        subscribeUI()
        initUI()
    }

    private fun initUI() {
        binding.recyclerForecast.adapter = adapter
        viewModel.getCacheWeather(city)
        viewModel.getAndUpdateForecastWeather(city, 7, 5)
    }

    private fun subscribeUI() {
        viewModel.errors.observe(viewLifecycleOwner) {
            it.printStackTrace()
            Toast.makeText(
                requireContext(),
                getString(R.string.fetch_data_fail),
                Toast.LENGTH_SHORT
            ).show()
        }

        viewModel.loader.observe(viewLifecycleOwner) {
            Timber.e("$city Loader: $it")
            updateLoaderUI(it)
        }

        viewModel.forecastWeather.observe(viewLifecycleOwner) {
            Timber.e("ForecastWeatherInfo: $it")
            it?.let { displayData(it) }
        }
    }

    private fun displayData(weatherInfo: WeatherLocal) {
        binding.run {
            when (weatherInfo.weather_code) {
                179, 323, 326, 329, 332, 335, 338, 350, 353, 356, 374, 377, 386, 392, 395 ->
                    imageSun.setImageResource(R.drawable.snowflake)
                113, 116 ->
                    imageSun.setImageResource(R.drawable.cloudy_sun)
                176, 230, 266, 293, 296, 299, 302, 305, 308, 311, 314, 359, 362, 365, 368, 371, 389 ->
                    imageSun.setImageResource(R.drawable.rainy)
                119, 122, 143, 182, 185, 200, 227, 248, 260, 263, 281, 284, 320, 317 ->
                    imageSun.setImageResource(R.drawable.cloudy)
            }
            textCurrentWeather.text = "${weatherInfo.current_temp ?: 0} °C"
            textCurrentWind.text = "${weatherInfo.current_wind ?: 0} miles/h"
            textCurrentFeels.text = "${weatherInfo.current_feel ?: 0} °C"
        }
        adapter.addAll(weatherInfo.forecast.orEmpty())
    }
}