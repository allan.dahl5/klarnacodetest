package com.app.klarnacodetest.ui

import android.app.Application
import coil.Coil
import coil.ImageLoader
import com.app.klarnacodetest.BuildConfig
import com.app.klarnacodetest.common.di.*
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import timber.log.Timber

class KlarnaApp : Application() {

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) Timber.plant(MyDebugTree())
        startKoin {
            androidContext(this@KlarnaApp)

            modules(
                listOf(
                    appModules,
                    databaseModule,
                    networkModules,
                    repoModules,
                    viewModelModule
                )
            )
        }

        val imageLoader: ImageLoader by inject()
        Coil.setImageLoader(imageLoader)
    }

    class MyDebugTree : Timber.DebugTree() {
        override fun createStackElementTag(element: StackTraceElement): String? {
            return "KlarnaApp_(${element.fileName}:${element.lineNumber})#${element.methodName}"
        }
    }
}
