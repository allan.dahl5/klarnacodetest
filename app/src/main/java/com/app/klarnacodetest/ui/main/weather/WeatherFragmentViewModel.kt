package com.app.klarnacodetest.ui.main.weather

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.app.klarnacodetest.common.base.BaseViewModel
import com.app.klarnacodetest.common.data.database.entity.WeatherLocal
import kotlinx.coroutines.launch

class WeatherFragmentViewModel : BaseViewModel() {

    private val _forecastWeather = MutableLiveData<WeatherLocal>()
    val forecastWeather: LiveData<WeatherLocal> = _forecastWeather

    fun getCacheWeather(cityName: String) {
        viewModelScope.launch {
            processDataEvent(weatherRepo.getCachedForecast(cityName)) {
                _forecastWeather.postValue(it)
            }
        }
    }

    fun getAndUpdateForecastWeather(cityName: String, days: Int, hourly: Int) {
        displayLoader()
        viewModelScope.launch {
            processDataEvent(weatherRepo.getAndUpdateForecast(cityName, days, hourly)) {
                _forecastWeather.postValue(it)
            }
        }
    }
}
