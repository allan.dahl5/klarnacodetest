package com.app.klarnacodetest.ui.main

import androidx.viewpager2.adapter.FragmentStateAdapter
import com.app.klarnacodetest.ui.main.weather.WeatherFragment

class MainPagerAdapter(
    activity: MainActivity, private val fragmentList: ArrayList<WeatherFragment>
) : FragmentStateAdapter(activity) {

    override fun getItemCount() = fragmentList.size

    override fun createFragment(position: Int) = fragmentList[position]

}