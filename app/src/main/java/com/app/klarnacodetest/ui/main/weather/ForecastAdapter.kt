package com.app.klarnacodetest.ui.main.weather

import com.app.klarnacodetest.R
import com.app.klarnacodetest.common.base.BaseAdapter
import com.app.klarnacodetest.common.data.network.model.ForecastInfo
import com.app.klarnacodetest.databinding.ListItemForecastInfoBinding
import java.text.SimpleDateFormat
import java.util.*

class ForecastAdapter :
    BaseAdapter<ListItemForecastInfoBinding, ForecastInfo>(R.layout.list_item_forecast_info) {
    override fun setClickableView(binding: ListItemForecastInfoBinding) = listOf(binding.itemMain)

    override fun onBind(
        binding: ListItemForecastInfoBinding,
        position: Int,
        item: ForecastInfo,
        vararg payloads: MutableList<Any>
    ) {
        val sdf = SimpleDateFormat("dd MMM, EEE", Locale.getDefault())
        val dateFormat = Date((item.date_epoch ?: 0) * 1000)
        val weekday: String = sdf.format(dateFormat)


        binding.textDay.text = weekday
        binding.textMinMaxTemp.text =
            binding.root.context.getString(R.string._1_s_c_2_s_c, item.maxtemp, item.mintemp)

    }
} 
