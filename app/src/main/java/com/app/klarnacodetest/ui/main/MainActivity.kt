package com.app.klarnacodetest.ui.main

import android.os.Bundle
import com.app.klarnacodetest.R
import com.app.klarnacodetest.common.base.BaseActivity
import com.app.klarnacodetest.databinding.ActivityMainBinding
import com.app.klarnacodetest.ui.main.weather.WeatherFragment
import com.google.android.material.tabs.TabLayoutMediator

class MainActivity : BaseActivity<ActivityMainBinding>(R.layout.activity_main) {

    private lateinit var mainPagerAdapter: MainPagerAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initUI()
    }

    private fun initUI() {
        val listTitle = arrayListOf("Berlin", "Stockholm")
        val listFragment = arrayListOf(
            WeatherFragment.newInstance("Berlin"),
            WeatherFragment.newInstance("Stockholm")
        )
        mainPagerAdapter = MainPagerAdapter(this, listFragment)
        binding.viewpagerMain.adapter = mainPagerAdapter

        TabLayoutMediator(binding.tabLayoutMain, binding.viewpagerMain) { tab, position ->
            tab.text = listTitle[position]
        }.attach()

    }
}